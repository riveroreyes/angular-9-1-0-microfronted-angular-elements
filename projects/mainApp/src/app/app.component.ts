import { Component, ViewEncapsulation, OnInit, OnDestroy, ChangeDetectorRef, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})

export class AppComponent implements OnInit, OnDestroy {
  public heading = 'Another heading';

  constructor(private cd: ChangeDetectorRef) {

  }

  @Input() public authToken: string;

  ngOnInit(): void {
    window.addEventListener('ce_subApp', this.customEventListenerFuntion.bind(this), true);
  }


  customEventListenerFuntion(event){
    this.heading = event.detail.action;
    this.cd.detectChanges();
  }

  ngOnDestroy(): void {
    window.removeEventListener('ce_subApp', this.customEventListenerFuntion, true);
  }
}
